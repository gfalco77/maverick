package com.gfs.maverick.api;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.gfs.maverick.AbstractContainerBaseTest;
import com.gfs.maverick.data.Product;

import static com.gfs.maverick.TestConstants.PRODUCT_DESCRIPTION;
import static com.gfs.maverick.TestConstants.PRODUCT_NAME;
import static com.gfs.maverick.TestConstants.PRODUCT_PRICE;
import static com.gfs.maverick.TestConstants.PRODUCT_SKU;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerTest extends AbstractContainerBaseTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    @Order(1)
    void testPostIsSuccessful() {
        createProductPostTest();
    }

    @Test
    @Order(2)
    void testGetProductIsSuccessful() {
        this.webTestClient.get()
                .uri(uriBuilder -> {
                    uriBuilder.path("/products/{id}");
                    return uriBuilder.build(PRODUCT_SKU);
                })
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.sku").isEqualTo(PRODUCT_SKU)
                .jsonPath("$.name").isEqualTo(PRODUCT_NAME)
                .jsonPath("$.description").isEqualTo(PRODUCT_DESCRIPTION)
                .jsonPath("$.price").isEqualTo(PRODUCT_PRICE);
    }

    @Test
    @Order(3)
    void testGetProductsIsSuccessful() {
        var product = createProductStub();
        this.webTestClient.get()
                .uri("/products")
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(Product.class)
                .hasSize(1)
                .contains(product);
    }

    @Test
    @Order(4)
    void testGetProductIsNotFound() {
        this.webTestClient.get()
                .uri(uriBuilder -> {
                    uriBuilder.path("/products/{id}");
                    return uriBuilder.build("anyString");
                })
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    @Order(5)
    void testUpdateProductIsUpdatedExists() {
        this.webTestClient.put()
                .uri(uriBuilder -> {
                    uriBuilder.path("/products/{id}");
                    return uriBuilder.build(PRODUCT_SKU);
                })
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .body(Mono.just(createProductStub()), Product.class)
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.sku").isEqualTo(PRODUCT_SKU)
                .jsonPath("$.name").isEqualTo(PRODUCT_NAME)
                .jsonPath("$.description").isEqualTo(PRODUCT_DESCRIPTION)
                .jsonPath("$.price").isEqualTo(PRODUCT_PRICE);
    }

    @Test
    @Order(6)
    void testUpdateProductReturnsConflictIfSkuAndBodySkuAreDifferent() {
        this.webTestClient.put()
                .uri(uriBuilder -> {
                    uriBuilder.path("/products/{id}");
                    return uriBuilder.build("anyString");
                })
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .body(Mono.just(createProductStub()), Product.class)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.CONFLICT);
    }

    @Test
    @Order(7)
    void testUpdateProductReturnsOptimisticLockIfVersionIsDifferent() {
        var product = createProductStub();
        product.setVersion(3L);
        this.webTestClient.put()
                .uri(uriBuilder -> {
                    uriBuilder.path("/products/{id}");
                    return uriBuilder.build(PRODUCT_SKU);
                })
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .body(Mono.just(product), Product.class)
                .exchange()
                .expectStatus()
                .is5xxServerError();
    }

    @Test
    @Order(8)
    void testPartialUpdateProductIsUpdatedExists() {
        this.webTestClient.patch()
                .uri(uriBuilder -> {
                    uriBuilder.path("/products/{id}");
                    return uriBuilder.build(PRODUCT_SKU);
                })
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .body(Mono.just(Product.builder()
                        .sku(PRODUCT_SKU)
                        .price(new BigDecimal("1.3"))
                        .version(1L)
                        .build()), Product.class)
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.sku").isEqualTo(PRODUCT_SKU)
                .jsonPath("$.name").isEmpty()
                .jsonPath("$.description").isEmpty()
                .jsonPath("$.price").isEqualTo(new BigDecimal("1.3"));
    }

    @Test
    @Order(9)
    void testPartialUpdateProductReturnsConflictIfSkuAndBodySkuAreDifferent() {
        this.webTestClient.patch()
                .uri(uriBuilder -> {
                    uriBuilder.path("/products/{id}");
                    return uriBuilder.build("anyString");
                })
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .body(Mono.just(createProductStub()), Product.class)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.CONFLICT);
    }


    @Test
    @Order(10)
    void testDeleteProductReturnsSuccess() {
        this.webTestClient.delete()
                .uri(uriBuilder -> {
                    uriBuilder.path("/products/{id}");
                    return uriBuilder.build(PRODUCT_SKU);
                })
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .exchange()
                .expectStatus()
                .isOk();
    }


    private void createProductPostTest() {
        this.webTestClient
                .post()
                .uri("/products")
                .header(ACCEPT, APPLICATION_JSON_VALUE)
                .body(Mono.just(createProductStub()), Product.class)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectHeader()
                .contentType(APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.sku").isEqualTo(PRODUCT_SKU)
                .jsonPath("$.name").isEqualTo(PRODUCT_NAME)
                .jsonPath("$.description").isEqualTo(PRODUCT_DESCRIPTION)
                .jsonPath("$.price").isEqualTo(PRODUCT_PRICE);
    }

    private Product createProductStub() {
        return Product.builder()
                .sku(PRODUCT_SKU)
                .description(PRODUCT_DESCRIPTION)
                .name(PRODUCT_NAME)
                .price(PRODUCT_PRICE)
                .version(0L)
                .build();
    }
}
