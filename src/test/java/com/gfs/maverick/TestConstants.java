package com.gfs.maverick;

import java.math.BigDecimal;

public final class TestConstants {

    public static final String PRODUCT_SKU = "sku";
    public static final String PRODUCT_DESCRIPTION = "description";
    public static final String PRODUCT_NAME = "name";
    public static final BigDecimal PRODUCT_PRICE = BigDecimal.TEN;
}
