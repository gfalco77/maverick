package com.gfs.maverick.repositories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.OptimisticLockingFailureException;

import com.gfs.maverick.AbstractContainerBaseTest;
import com.gfs.maverick.models.ProductModel;

import static com.gfs.maverick.TestConstants.PRODUCT_SKU;
import static org.junit.jupiter.api.Assertions.assertEquals;

import reactor.test.StepVerifier;

import java.math.BigDecimal;

@SpringBootTest
class ProductRepositoryIT extends AbstractContainerBaseTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    void shouldReturnProductCreated() {
        var productModel = ProductModel.builder().sku(PRODUCT_SKU).newProduct(true).build();
        productRepository.save(productModel).subscribe();

        var product = productRepository.findBySku(PRODUCT_SKU);

        StepVerifier.create(product).expectNextMatches(p -> {
            assertEquals(PRODUCT_SKU, p.getSku());
            return true;
        }).verifyComplete();
    }

    @Test
    void deleteProduct() {
        var productModel = ProductModel.builder().sku(PRODUCT_SKU).newProduct(true).build();
        productRepository.save(productModel).subscribe();

        var product = productRepository.findBySku(PRODUCT_SKU);

        StepVerifier.create(product).expectNextMatches(p -> {
            assertEquals(PRODUCT_SKU, p.getSku());
            return true;
        }).verifyComplete();

        StepVerifier.create(productRepository.deleteById(PRODUCT_SKU)).verifyComplete();
    }
}
