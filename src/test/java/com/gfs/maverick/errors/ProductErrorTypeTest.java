package com.gfs.maverick.errors;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static com.gfs.maverick.errors.ProductErrorType.PRODUCTS_NOT_FOUND;
import static com.gfs.maverick.errors.ProductErrorType.PRODUCT_NOT_FOUND;
import static org.assertj.core.api.Assertions.assertThat;

class ProductErrorTypeTest {

    @Test
    void testProductNotFoundError() {
        assertThat(PRODUCT_NOT_FOUND.getHttpStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(PRODUCT_NOT_FOUND.getDescription()).isEqualTo("No product with sku [%s] can be found");
        assertThat(PRODUCT_NOT_FOUND.getMessage()).isEqualTo("Not found");
    }

    @Test
    void testProductsNotFoundError() {
        assertThat(PRODUCTS_NOT_FOUND.getHttpStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(PRODUCTS_NOT_FOUND.getDescription()).isEqualTo("No products found");
        assertThat(PRODUCTS_NOT_FOUND.getMessage()).isEqualTo("Not found");
    }

}
