package com.gfs.maverick.services;

import com.gfs.gorgon.exceptions.GorgonRuntimeException;
import com.gfs.maverick.data.Product;
import com.gfs.maverick.mappers.ProductMapper;
import com.gfs.maverick.models.ProductModel;
import com.gfs.maverick.repositories.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;

import static com.gfs.maverick.TestConstants.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DefaultProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private DefaultProductService defaultProductService;

    @Test
    void testCreateProductIsSuccessful() {
        Product product = createProductStub();
        ProductModel productModel = createProductModelStub();

        when(productRepository.save(productModel)).thenReturn(Mono.just(productModel));

        var result = defaultProductService.create(product);

        StepVerifier.create(result).expectNext(product).verifyComplete();
    }

    @Test
    void testGetProductIsSuccessful() {
        Product product = createProductStub();
        ProductModel productModel = createProductModelStub();

        when(productRepository.findBySku(PRODUCT_SKU)).thenReturn(Mono.just(productModel));

        var result = defaultProductService.get(PRODUCT_SKU);

        StepVerifier.create(result).expectNext(product).verifyComplete();
    }

    @Test
    void testGetProductsIsSuccessful() {
        Product product = createProductStub();
        ProductModel productModel = createProductModelStub();

        when(productRepository.findAll()).thenReturn(Flux.just(productModel));

        var result = defaultProductService.get();

        StepVerifier.create(result).expectNext(product).verifyComplete();
    }

    @Test
    void testProductIsUpdatedIfProductAlreadyExists() {
        var product = Product.builder()
                .sku(PRODUCT_SKU)
                .price(new BigDecimal("1.33"))
                .build();


        var productModel = createProductModelStub();
        when(productRepository.findBySku(PRODUCT_SKU)).thenReturn(Mono.just(productModel));

        var productModelUpdated = ProductMapper.mapper.toProductModel(product);
        when(productRepository.save(productModelUpdated)).thenReturn(Mono.just(productModelUpdated));

        var result = defaultProductService.update(product);

        StepVerifier.create(result)
                .expectNextMatches(productUpdated -> {
                    Assertions.assertEquals(BigDecimal.valueOf(1.33), productUpdated.getPrice());
                    Assertions.assertNull(productUpdated.getDescription());
                    Assertions.assertNull(productUpdated.getName());
                    Assertions.assertEquals(PRODUCT_SKU, productUpdated.getSku());
                    return true;
                }).verifyComplete();

        verify(productRepository, times(1)).findBySku(PRODUCT_SKU);
        verify(productRepository, times(1)).save(productModelUpdated);
    }

    @Test
    void testProductIsPartiallyUpdatedIfProductAlreadyExists() {
        var product = createProductStub();
        product.setPrice(new BigDecimal("1.33"));

        var productModel = createProductModelStub();
        when(productRepository.findBySku(PRODUCT_SKU)).thenReturn(Mono.just(productModel));

        var productModelUpdated = ProductMapper.mapper.patchProductModel(productModel, product);
        when(productRepository.save(productModelUpdated)).thenReturn(Mono.just(productModelUpdated));

        var result = defaultProductService.partialUpdate(product);

        StepVerifier.create(result)
                .expectNextMatches(productUpdated -> {
                    Assertions.assertEquals(BigDecimal.valueOf(1.33), productUpdated.getPrice());
                    Assertions.assertEquals(PRODUCT_DESCRIPTION, productUpdated.getDescription());
                    Assertions.assertEquals(PRODUCT_NAME, productUpdated.getName());
                    Assertions.assertEquals(PRODUCT_SKU, productUpdated.getSku());
                    return true;
                }).verifyComplete();

        verify(productRepository, times(1)).findBySku(PRODUCT_SKU);
        verify(productRepository, times(1)).save(productModelUpdated);
    }

    @Test
    void testProductIsNotUpdatedIfItDoesNotExists() {
        Product product = createProductStub();
        product.setPrice(new BigDecimal("1.33"));
        product.setVersion(0L);

        when(productRepository.findBySku(PRODUCT_SKU)).thenReturn(Mono.empty());

        var result = defaultProductService.update(product);

        StepVerifier.create(result)
                .expectErrorMatches(throwable -> throwable instanceof GorgonRuntimeException &&
                        throwable.getMessage().equals("No product with sku [sku] can be found"))
                .verify();
    }

    @Test
    void testProductIsNotPartiallyUpdatedIfItDoesNotExists() {
        Product product = createProductStub();
        product.setPrice(new BigDecimal("1.33"));

        when(productRepository.findBySku(PRODUCT_SKU)).thenReturn(Mono.empty());

        var result = defaultProductService.partialUpdate(product);

        StepVerifier.create(result)
                .expectErrorMatches(throwable -> throwable instanceof GorgonRuntimeException &&
                        throwable.getMessage().equals("No product with sku [sku] can be found"))
                .verify();
    }

    @Test
    void testProductIsDeleted() {
        when(productRepository.deleteById(PRODUCT_SKU)).thenReturn(Mono.empty());
        var result = defaultProductService.delete(PRODUCT_SKU);
        StepVerifier.create(result).verifyComplete();
    }

    private Product createProductStub() {
        return Product.builder()
                .sku(PRODUCT_SKU)
                .description(PRODUCT_DESCRIPTION)
                .name(PRODUCT_NAME)
                .price(PRODUCT_PRICE)
                .build();
    }

    private ProductModel createProductModelStub() {
        return ProductModel.builder()
                .sku(PRODUCT_SKU)
                .description(PRODUCT_DESCRIPTION)
                .name(PRODUCT_NAME)
                .newProduct(true)
                .price(PRODUCT_PRICE)
                .build();
    }
}
