package com.gfs.maverick;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.annotation.EnableRetry;


@ComponentScan("com.gfs")
@EnableRetry
@SpringBootApplication(exclude={MongoAutoConfiguration.class})
public class MaverickApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaverickApplication.class, args);
    }
}
