package com.gfs.maverick.mappers;

import java.util.List;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import com.gfs.maverick.data.Product;
import com.gfs.maverick.models.ProductModel;

@Mapper
public interface ProductMapper {

    ProductMapper mapper = Mappers.getMapper(ProductMapper.class);

    Product toProduct(ProductModel productModel);

    ProductModel toProductModel(Product product);

    @BeanMapping(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    ProductModel patchProductModel(@MappingTarget ProductModel productModel, Product product);

    List<Product> toProducts(List<ProductModel> productModels);
}
