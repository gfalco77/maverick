package com.gfs.maverick.migration;

import com.mongodb.reactivestreams.client.ClientSession;
import com.mongodb.reactivestreams.client.MongoDatabase;

import static com.gfs.maverick.config.ProductDbConfiguration.PRODUCT_COLLECTION;
import io.mongock.api.annotations.BeforeExecution;
import io.mongock.api.annotations.ChangeUnit;
import io.mongock.api.annotations.Execution;
import io.mongock.api.annotations.RollbackBeforeExecution;
import io.mongock.api.annotations.RollbackExecution;
import io.mongock.driver.mongodb.reactive.util.MongoSubscriberSync;
import io.mongock.driver.mongodb.reactive.util.SubscriberSync;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ChangeUnit(order = "1", id = "product-initializer")
public class ProductInitializerChangeUnit {

    @RollbackExecution
    public void rollbackExecution(MongoDatabase mongoDatabase) {
    }

    @Execution
    public void execution(ClientSession clientSession, MongoDatabase mongoDatabase) {
    }

    @BeforeExecution
    public void beforeExecution(MongoDatabase mongoDatabase) {
        SubscriberSync<Void> subscriber = new MongoSubscriberSync<>();
        mongoDatabase.createCollection(PRODUCT_COLLECTION).subscribe(subscriber);
        subscriber.await();
    }

    @RollbackBeforeExecution
    public void rollbackBeforeExecution(MongoDatabase mongoDatabase) {
        SubscriberSync<Void> subscriber = new MongoSubscriberSync<>();
        mongoDatabase.getCollection(PRODUCT_COLLECTION).drop().subscribe(subscriber);
        subscriber.await();
    }
}
