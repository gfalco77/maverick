package com.gfs.maverick.models;

import java.util.List;
import java.util.Set;

import org.bson.BsonType;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonRepresentation;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import static com.gfs.maverick.config.ProductDbConfiguration.CATEGORY_COLLECTION;
import lombok.Data;

@Data
@Document(collection = CATEGORY_COLLECTION)
public class CategoryModel {

    @Id
    private Integer id;

    private String name;
    private String description;
    private List<MediaModel> images;

    @DocumentReference(lazy=true)
    private Set<CategoryModel> superCategories;

    @DocumentReference(lazy=true)
    private Set<CategoryModel> subCategories;

    @DocumentReference
    private Set<ProductModel> products;
}
