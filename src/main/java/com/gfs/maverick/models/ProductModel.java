package com.gfs.maverick.models;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import static com.gfs.maverick.config.ProductDbConfiguration.PRODUCT_COLLECTION;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = PRODUCT_COLLECTION)
public class ProductModel implements Persistable<String> {

    @Id
    private String sku;

    private String name;
    private String description;
    private List<ReviewModel> reviews;
    private Double reviewRating;
    private Map<String, String> specifications;
    private List<MediaModel> images;
    private BigDecimal price;
    @Version
    private Long version;

    @DocumentReference(lazy = true)
    private Set<CategoryModel> categories;

    // used for migration and when insert with natural key
    @Transient
    private boolean newProduct;

    @Override
    public String getId() {
        return sku;
    }

    @Override
    public boolean isNew() {
        return newProduct;
    }
}
