package com.gfs.maverick.models;

public enum MediaType {

    THUMBNAIL, ZOOM, PRODUCT, MAIN_PRODUCT,  // images
    VIDEO, // video
    AUDIO  // audio
}
