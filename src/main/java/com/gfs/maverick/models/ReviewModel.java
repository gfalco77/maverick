package com.gfs.maverick.models;

import java.time.OffsetDateTime;

import lombok.Data;

@Data
public class ReviewModel {

    private Double rating;
    private String description;
    private String title;
    private OffsetDateTime date;
}
