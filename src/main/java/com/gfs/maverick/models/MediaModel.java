package com.gfs.maverick.models;

import lombok.Data;

@Data
public class MediaModel {

    private String name;
    private String path;
    private MediaType type;
}
