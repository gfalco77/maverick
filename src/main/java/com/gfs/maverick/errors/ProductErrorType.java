package com.gfs.maverick.errors;

import org.springframework.http.HttpStatus;

import com.gfs.gorgon.ErrorType;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ProductErrorType implements ErrorType {

    PRODUCT_NOT_FOUND(HttpStatus.NOT_FOUND, "Not found", "No product with sku [%s] can be found"),
    PRODUCTS_NOT_FOUND(HttpStatus.NOT_FOUND, "Not found", "No products found"),
    CONFLICT(HttpStatus.CONFLICT, "Conflict", "The data provided cannot be processed because of a conflict with existing data");

    private final HttpStatus httpStatus;
    private final String message;
    private final String description;

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
