package com.gfs.maverick.services;

import com.gfs.gorgon.exceptions.GorgonRuntimeException;
import com.gfs.maverick.data.Product;
import com.gfs.maverick.mappers.ProductMapper;
import com.gfs.maverick.repositories.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import static com.gfs.maverick.errors.ProductErrorType.PRODUCTS_NOT_FOUND;
import static com.gfs.maverick.errors.ProductErrorType.PRODUCT_NOT_FOUND;

@Slf4j
@Service
@RequiredArgsConstructor
public class DefaultProductService implements ProductService {

    private final ProductRepository productRepository;

    @Value("${maverick.locking.retry.times}")
    private int retryTimes;

    @Value("${maverick.locking.retry.delayInMillis}")
    private int retryDelayInMillis;

    @Override
    public Mono<Product> create(Product product) {
        var productModel = ProductMapper.mapper.toProductModel(product);
        productModel.setNewProduct(true);
        return productRepository.save(productModel).map(ProductMapper.mapper::toProduct);
    }

    @Override
    public Mono<Product> update(Product product) {
        return productRepository.findBySku(product.getSku())
                .switchIfEmpty(Mono.defer(() -> Mono.error(new GorgonRuntimeException(PRODUCT_NOT_FOUND, product.getSku()))))
                .flatMap(existingProduct -> productRepository.save(ProductMapper.mapper.toProductModel(product)))
                .retryWhen(Retry.backoff(retryTimes, Duration.of(retryDelayInMillis, ChronoUnit.MILLIS))
                        .filter(OptimisticLockingFailureException.class::isInstance))
                .map(ProductMapper.mapper::toProduct);
    }

    @Override
    public Mono<Product> partialUpdate(Product product) {
        return productRepository.findBySku(product.getSku())
                .switchIfEmpty(Mono.defer(() -> Mono.error(new GorgonRuntimeException(PRODUCT_NOT_FOUND, product.getSku()))))
                .flatMap(existingProduct -> productRepository.save(ProductMapper.mapper.patchProductModel(existingProduct, product)))
                .retryWhen(Retry.backoff(retryTimes, Duration.of(retryDelayInMillis, ChronoUnit.MILLIS))
                        .filter(OptimisticLockingFailureException.class::isInstance))
                .map(ProductMapper.mapper::toProduct);
    }

    @Override
    public Mono<Void> delete(String sku) {
        return productRepository.deleteById(sku);
    }

    @Override
    public Mono<Product> get(String sku) {
        return productRepository.findBySku(sku)
                .switchIfEmpty(Mono.defer(() -> Mono.error(new GorgonRuntimeException(PRODUCT_NOT_FOUND, sku))))
                .map(ProductMapper.mapper::toProduct);
    }

    @Override
    public Flux<Product> get() {
        return productRepository.findAll()
                .switchIfEmpty(Mono.defer(() -> Mono.error(new GorgonRuntimeException(PRODUCTS_NOT_FOUND))))
                .map(ProductMapper.mapper::toProduct);
    }
}
