package com.gfs.maverick.services;

import com.gfs.maverick.data.Product;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductService {

    Mono<Product> create(Product product);

    Mono<Product> update(Product product);

    Mono<Product> partialUpdate(Product product);

    Mono<Void> delete(String sku);

    Mono<Product> get(String sku);

    Flux<Product> get();

}
