package com.gfs.maverick.repositories;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.gfs.maverick.models.ProductModel;

import static com.gfs.maverick.config.ProductDbConfiguration.PRODUCT_COLLECTION;
import reactor.core.publisher.Mono;

@Repository(PRODUCT_COLLECTION)
public interface ProductRepository extends ReactiveMongoRepository<ProductModel, String> {

    Mono<ProductModel> findBySku(String sku);
}
