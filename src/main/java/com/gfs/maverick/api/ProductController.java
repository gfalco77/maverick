package com.gfs.maverick.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

import com.gfs.gorgon.exceptions.GorgonRuntimeException;
import com.gfs.maverick.data.Product;
import com.gfs.maverick.errors.ProductErrorType;
import com.gfs.maverick.services.ProductService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@AllArgsConstructor
@Slf4j
public class ProductController implements ProductsApi {

    private final ProductService productService;

    @Override
    public Mono<ResponseEntity<Product>> createProduct(Mono<Product> product, ServerWebExchange exchange) {
        return product.flatMap(productService::create)
                      .doOnSuccess(productSaved -> log.debug("Product created, sku [{}]", productSaved.getSku()))
                      .map(productSaved -> new ResponseEntity<>(productSaved, HttpStatus.CREATED));
    }

    @Override
    public Mono<ResponseEntity<Flux<Product>>> getProducts(ServerWebExchange exchange) {
        return Mono.just(ResponseEntity.ok(productService.get()));
    }

    @Override
    public Mono<ResponseEntity<Product>> getProduct(String sku, ServerWebExchange exchange) {
        return productService.get(sku).map(ResponseEntity::ok);
    }

    // Difference between PUT ad PATCH is that PUT body will update all the properties. PATCH, only the ones that are set
    @Override
    public Mono<ResponseEntity<Product>> updateProduct(String sku, Mono<Product> product, ServerWebExchange exchange) {
        return product.flatMap(productFlatMapped -> {
                          if (!sku.equals(productFlatMapped.getSku())) {
                              return Mono.error(new GorgonRuntimeException(ProductErrorType.CONFLICT));
                          }
                          return productService.update(productFlatMapped);
                      })
                      .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<Product>> patchProduct(String sku, Mono<Product> product, ServerWebExchange exchange) {
        return product.flatMap(productFlatMapped -> {
                    if (!sku.equals(productFlatMapped.getSku())) {
                        return Mono.error(new GorgonRuntimeException(ProductErrorType.CONFLICT));
                    }
                    return productService.partialUpdate(productFlatMapped);
                })
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<Void>> deleteProduct(String sku, ServerWebExchange exchange) {
        return productService.get(sku)
                             .flatMap(product -> productService.delete(sku))
                             .map(ResponseEntity::ok);
    }
}
