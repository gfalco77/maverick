# Maverick  

Microservices that manage Products

Product is generic. In eCommerce you can have different types of products with extra properties

Start docker-compose up when running it locally

Implementation decisions
1. PUT and PATCH are using the same Product OAS where 'sku' and 'version' are mandatory
2. Both methods throw an optimisticLockException if version doesn't match or there's a concurrent update
3. PATCH is reusing product and a different mapstruct mapper method to ignore null properties. This way we avoid using a map of string, object and reflection
